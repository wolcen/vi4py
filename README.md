VI4PY - Configuring VIm for Python Development
==============================================

VIm is, as EVERYONE knows (:wink:) the single best text editor around. But, when it comes to supporting the features of even the most basic of IDEs, VI just feels naked out of the box.

My hope, with this project, is that I might cobble together the best set of plug-ins for VIm I can find, and fully configure them as a package that others may use as well, in order to provide a full IDE-like experience to more VIm users.

As the majority of my time is spent in Python, along with the typical stack of web technologies, the focus of this project is for these scripting and markup languages.

Current status
--------------

Presently, there is NO release. I am collecting various plug-ins to test/configure/and ultimately include, as I learn more about VIm plug-ins in general.

Some features I plan to add to this package include:
- Debugging
- Linting/syntax checking, automatic/on save
- Code-completion
- Git integration
- much more...

The very starting poing of this project at this point is expected to be sensible-vim, by tpope https://github.com/tpope/vim-sensible

I currently recommend the guide at https://anotherwayoflife.wordpress.com/2013/08/31/turning-vim-into-a-modern-python-ide/
